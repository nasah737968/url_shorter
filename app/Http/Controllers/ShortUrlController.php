<?php

namespace App\Http\Controllers;

use App\Http\Requests\ShortRequest;
use App\Models\ShortUrl;
use Illuminate\Support\Facades\RateLimiter;

class ShortUrlController extends Controller
{
    public function short(ShortRequest $request)
    {
        if ($request->original_url) {
            if (auth()->user()) {
                $new_url = auth()->user()->links()->create([
                    'original_url' => $request->original_url
                ]);
            } else {
                $new_url = ShortUrl::create([
                'original_url' => $request->original_url
            ]);
            }
            if ($new_url) {
                $short_url = $random = substr(md5(mt_rand()), 0, 6);
                $new_url->update([
                    'short_url' => $short_url
                ]);

                return back();
            }
        }
        
        return back();
    }

    public function show($code)
    {
        $short_url = ShortUrl::where('short_url', $code)->first();

        $max = 3;  //attempts
        $decay = 300; // 300 seconds/5 minute for suspending 

        if (RateLimiter::tooManyAttempts($code, $max)) {
            abort(403, 'You perform the short URL action 3 times a minute. So suspend your action for 5 minutes');
        } else {
            RateLimiter::hit($code, $decay);
        }

        if ($short_url) {
            $short_url->increment('visits');
            return redirect()->to(url($short_url->original_url));
        }

        return redirect()->to(url('/'));
    }
}