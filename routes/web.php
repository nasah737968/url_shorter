<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ShortUrlController;
use App\Http\Controllers\ShortUserController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/links', [ShortUserController::class, 'index'])->name('user.links')->middleware('auth');

Route::post('/short', [ShortUrlController::class, 'short'])->name('short.url');
Route::get('/{code}', [ShortUrlController::class, 'show'])->name('short.show'); // ->middleware('throttle:3,1')  Action per min 3 time
