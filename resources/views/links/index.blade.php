@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Url Shorter') }}</div>

                <div class="card-body">

                    {{-- start url shorter form --}}
                    <form method="POST" action="{{ route('short.url') }}">
                        @csrf

                        <div class="row mb-3">
                            <div class="col-md-10">
                                <input class="form-control" type="text" name="original_url" placeholder="Enter valid Short URL" />
                                @error('original_url')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="col-md-2">
                                <button class="btn btn-primary" type="submit">Short</button>
                            </div>
                        </div>
                    </form>
                    {{-- end url shorter form --}}

                    {{-- start short url list --}}
                    <div class="table-responsive">
                        <table class="table table-bordered">
                           <tr>
                               <th>SL</th>
                               <th>Original URL</th>
                               <th>Short URL</th>
                               <th>Visits Count</th>
                           </tr>
                           @foreach ($links as $link)
                              <tr>
                                <td>{{ $link->id }}</td>
                                <td>{{ $link->original_url}}</td>
                                <td><a href="{{ url($link->short_url) }}" target="_blank">{{$link->short_url}}</a></td>
                                <td>{{ $link->visits }}</td>
                              </tr>
                          @endforeach
                        </table> 
                       <div>{{ $links->links() }}</div>
                   </div>
                   {{-- end short url list --}}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
